package com.example.myapplication.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DateHelper {
    private final static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MMM-yyyy");

    public static Date currentDate() {
        try {
            Date todaysDate = DATE_FORMATTER.parse(DATE_FORMATTER.format(new Date()));
            return todaysDate;
        } catch (Exception ex) {
            Logger.getAnonymousLogger().log(Level.WARNING,"Date couldn't be parsed");
            return new Date();
        }
    }

    public static Date addDays(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE,days);
        return calendar.getTime();
    }

    public static Date changeDateFormat(Date date) {
        try {
            return DATE_FORMATTER.parse(DATE_FORMATTER.format(date));
        } catch (Exception ex) {
            Logger.getAnonymousLogger().log(Level.WARNING,"Date couldn't be parsed");
            return date;
        }
    }

}
