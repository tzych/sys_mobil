package com.example.myapplication.ui.RecyclerViewAdapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myapplication.R;
import com.example.myapplication.model.Product;
import com.example.myapplication.service.WineManager;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private Context context;
    private ArrayList<Product> products;
    private WineManager manager;
    private ViewGroup parentView;

    public ProductAdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
        manager = new WineManager(context);
    }

    @NonNull
    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        parentView = parent;
        View view = inflater.inflate(R.layout.row_item_for_ready_product, parent, false);
        return new ProductAdapter.ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.name.setText(context.getString(R.string.ready_product_name) + " " + product.getName());
        holder.description.setText(context.getString(R.string.ready_product_description) + " " + String.valueOf(product.getDescription()));
        holder.amount.setText(String.valueOf(product.getAmount()));
        holder.button_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.getAmount() >= 1) {
                    product.setAmount((product.getAmount() - 1));
                    manager.updateProduct(product);
                    holder.amount.setText(String.valueOf(product.getAmount()));
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setTitle("Product : " + product.getName());
                    alertDialog.setMessage("There is 0 products already. What do to ?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            manager.deleteProduct(product.getId());
                            alertDialog.dismiss();
                            Snackbar.make(parentView, "Deleted. Please refresh values.", BaseTransientBottomBar.LENGTH_SHORT).setTextColor(Color.BLACK).show();
                        } });

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Go back", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }});

                    alertDialog.show();
                }
            }
        });
        holder.button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product.setAmount((product.getAmount() + 1));
                manager.updateProduct(product);
                holder.amount.setText(String.valueOf(product.getAmount()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{
        TextView name, description, amount;
        Button button_remove, button_add;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.product_name);
            description = itemView.findViewById(R.id.product_description);
            amount = itemView.findViewById(R.id.product_amount);
            button_remove = itemView.findViewById(R.id.button_remove);
            button_add = itemView.findViewById(R.id.button_add);
        }
    }
}