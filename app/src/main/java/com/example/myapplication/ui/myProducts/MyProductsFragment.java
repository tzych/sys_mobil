package com.example.myapplication.ui.myProducts;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class MyProductsFragment extends Fragment {
    private RecyclerView myProducts;
    private WineManager wineManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.my_products_section_layout, container, false);
        wineManager = new WineManager(getContext());
        myProducts = root.findViewById(R.id.ready_products_recycler);
        refreshViews();

        View floatingPlus = getActivity().findViewById(R.id.fab);
        floatingPlus.setVisibility(View.VISIBLE);
        floatingPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.popup_new_product,null, false);
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setCancelable(false)
                        .setTitle(getContext().getString(R.string.new_product_title))
                        .setView(viewInflated)
                        .setPositiveButton(android.R.string.ok,null)
                        .setNegativeButton(android.R.string.cancel,null)
                        .show();

                final EditText newProductName = viewInflated.findViewById(R.id.popup_add_product_name);
                final EditText newProductDescription = viewInflated.findViewById(R.id.popup_add_product_description);
                final EditText newProductAmount = viewInflated.findViewById(R.id.popup_add_product_amount);

                Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String prodName = newProductName.getText().toString();
                        String prodDescription = newProductDescription.getText().toString();
                        String prodAmount = newProductAmount.getText().toString();
                        String validationResult = validateNewProduct(prodName, prodDescription, prodAmount);
                        if (validationResult.isEmpty()) {
                            wineManager.addNewProduct(prodName, prodDescription,Integer.valueOf(prodAmount));
                            alertDialog.dismiss();
                        } else {
                            Snackbar.make(viewInflated, validationResult, BaseTransientBottomBar.LENGTH_SHORT).setTextColor(Color.BLACK).show();
                        }
                        refreshViews();
                    }
                });

                Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
            }
        });
        return root;
    }

    private void refreshViews() {
        ProductAdapter productAdapter = new ProductAdapter(getContext(), wineManager.getAllProducts());
        myProducts.setAdapter(productAdapter);
        myProducts.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public String validateNewProduct(String name, String desc, String amount) {
        StringBuilder sb = new StringBuilder();
        if (name == null || name.trim().isEmpty()) {
            sb.append(getContext().getString(R.string.new_product_validate_name));
            sb.append("\n");
        }

        if (desc == null || desc.trim().isEmpty()) {
            sb.append(getContext().getString(R.string.new_product_validate_desc));
            sb.append("\n");
        }

        try {
            int productAmount = Integer.valueOf(amount);
            if (productAmount <= 0) {
                sb.append(getContext().getString(R.string.new_product_validate_amount));
                sb.append("\n");
            }
        } catch (Exception ex){
            sb.append(getContext().getString(R.string.new_product_validate_amount));
            sb.append("\n");
        }

        return sb.toString();
    }
}