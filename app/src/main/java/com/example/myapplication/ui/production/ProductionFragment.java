package com.example.myapplication.ui.production;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.ProductionDetails;
import com.example.myapplication.R;
import com.example.myapplication.model.Production;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductionAdapter;
import com.example.myapplication.ui.newProduction.NewProduction;

import java.util.ArrayList;


public class ProductionFragment extends Fragment implements ProductionAdapter.ListItemClickListener{
    private ArrayList<Production> productions;
    private RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.productions_section_layout, container, false);
        recyclerView = root.findViewById(R.id.production_recycler_view);

        WineManager manager = new WineManager(getContext());
        productions = manager.getMySortedProductions();
        ProductionAdapter prodAdapter = new ProductionAdapter(getContext(),productions,this);
        recyclerView.setAdapter(prodAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        View floatingPlus = getActivity().findViewById(R.id.fab);
        floatingPlus.setVisibility(View.VISIBLE);
        floatingPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newProd = new Intent(getActivity(), NewProduction.class);
                startActivity(newProd);
            }
        });
        return root;
    }

    @Override
    public void onListItemClick(int position) {
        Intent productionDetail = new Intent(getActivity(), ProductionDetails.class);
        productionDetail.putExtra("ProductionID",productions.get(position).getId());
        startActivity(productionDetail);
    }
}