package com.example.myapplication.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.ProductionDetails;
import com.example.myapplication.R;
import com.example.myapplication.model.CalendarAction;
import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.service.DAO.FakeValueProvider;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.CalendarAdapter;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductionAdapter;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;

public class HomeFragment extends Fragment implements ProductionAdapter.ListItemClickListener, CalendarAdapter.CalendarItemClickListener {
    private ArrayList<Production> lastProduction;
    private RecyclerView recyclerView, calendarRecyclerView;
    private ArrayList<CalendarAction> calendarActions;
    private ArrayList<CalendarAction> actions;
    private WineManager manager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.home_section_layout, container, false);
        View floatingPlus = getActivity().findViewById(R.id.fab);
        floatingPlus.setVisibility(INVISIBLE);
        manager = new WineManager(getContext());
        if (manager.getMyProductions().isEmpty()) {
            FakeValueProvider.loadSampleData(getContext());
        }
        findViewElements(root);
        fillElementsWithValues();
        return root;
    }

    private void findViewElements(View root) {
        recyclerView = root.findViewById(R.id.home_last_production_recycler);
        calendarRecyclerView = root.findViewById(R.id.home_calendar_recycler);
    }

    private void fillElementsWithValues() {
        lastProduction = new ArrayList<>();
        lastProduction.add(manager.getLastProduction());
        ProductionAdapter prodAdapter = new ProductionAdapter(getContext(), lastProduction,this);
        recyclerView.setAdapter(prodAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        actions = manager.getFutureActions();
        CalendarAdapter calendarAdapter = new CalendarAdapter(getContext(), actions,this);
        calendarRecyclerView.setAdapter(calendarAdapter);
        calendarRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    @Override
    public void onListItemClick(int position) {
        Intent productionDetail = new Intent(getActivity(), ProductionDetails.class);
        productionDetail.putExtra("ProductionID",lastProduction.get(position).getId());
        startActivity(productionDetail);
    }
    @Override
    public void onCalendarListItemClick(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setCancelable(false);
        CalendarAction selected = actions.get(position);
        alertDialog.setTitle("Action : " + selected.getActionType().getDesc());
        alertDialog.setMessage("Select what to do with action");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                manager.deleteCalendarProductionAction(selected.getProductionActionId());
                alertDialog.dismiss();
            } });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Go back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }});

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Finish", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                manager.updateProductionActionToDone(selected.getProductionActionId());
                dialog.dismiss();
            }});

        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fillElementsWithValues();
            }
        });
    }
}