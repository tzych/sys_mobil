package com.example.myapplication.ui.RecyclerViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.CalendarAction;

import java.util.ArrayList;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.CalendarViewHolder>  {
    private Context context;
    private ArrayList<CalendarAction> calendarActions;
    final private CalendarItemClickListener onCalendarClickListener;

    public interface CalendarItemClickListener{
        void onCalendarListItemClick(int position);
    }

    public CalendarAdapter(Context context, ArrayList<CalendarAction> actions, CalendarAdapter.CalendarItemClickListener onCalendarClickListener) {
        this.context = context;
        this.calendarActions = actions;
        this.onCalendarClickListener = onCalendarClickListener;
    }

    @NonNull
    @Override
    public CalendarAdapter.CalendarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_item_for_calendar_action, parent, false);
        return new CalendarAdapter.CalendarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CalendarAdapter.CalendarViewHolder holder, int position) {
        CalendarAction action = calendarActions.get(position);
        holder.image.setImageResource(action.getProductionImage());
        holder.productionName.setText(action.getProductionName());
        holder.productionActionType.setText(context.getString(R.string.calendar_production_action_type) + " " + action.getActionType().getDesc());
        holder.productionActionDate.setText(context.getString(R.string.calendar_production_action_date) + " " + action.getActionDate().toString());

    }

    @Override
    public int getItemCount() {
        return calendarActions.size();
    }

    public class CalendarViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView image;
        TextView productionName, productionActionType, productionActionDate;

        public CalendarViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.calendar_production_image);
            productionName = itemView.findViewById(R.id.calendar_production_name);
            productionActionType = itemView.findViewById(R.id.calendar_production_action_type);
            productionActionDate = itemView.findViewById(R.id.calendar_production_action_date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            onCalendarClickListener.onCalendarListItemClick(position);
        }
    }
}
