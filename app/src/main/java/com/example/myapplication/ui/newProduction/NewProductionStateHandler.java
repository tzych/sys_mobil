package com.example.myapplication.ui.newProduction;

import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.ui.newProductionAction.ProductionActionStateHandler;

import java.util.ArrayList;
import java.util.Date;

public class NewProductionStateHandler {
    private static NewProductionStateHandler instance;
    private String prodName;
    private String mainResource;
    private String amount;
    private String power;
    private static ArrayList<ProductionAction> actions;
    private ProductionActionStateHandler _actionsStatehandler;

    public static final NewProductionStateHandler getInstance() {
        if (instance == null) {
           instance = new NewProductionStateHandler();
        }
        return instance;
    }

    public NewProductionStateHandler() {
        actions = new ArrayList<ProductionAction>();
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getMainResource() {
        return mainResource;
    }

    public void setMainResource(String mainResource) {
        this.mainResource = mainResource;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public ArrayList<ProductionAction> getActions() {
        return actions;
    }

    public void addAction(ProductionAction action) {
        this.actions.add(action);
    }

    public void removeAction(ProductionAction action) {
        this.actions.remove(action);
    }


    public void clearNewProductionStateHandlerData() {
        this.prodName = "";
        this.mainResource = "";
        this.amount = "";
        this.power = "";
        this.actions.clear();
    }
}
