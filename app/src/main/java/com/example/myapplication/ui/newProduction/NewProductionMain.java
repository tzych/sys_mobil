 package com.example.myapplication.ui.newProduction;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.IngredientAdapter;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductionActionAdapter;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductionAdapter;
import com.example.myapplication.ui.newProductionAction.NewProductionActionDialog;
import com.example.myapplication.ui.newProductionAction.ProductionActionContext;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.StringJoiner;

import static com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG;

public class NewProductionMain extends Fragment implements ProductionActionAdapter.ProductionActionClickListener {
    private NewProductionStateHandler _stateHandler;
    private TextInputEditText new_production_name_of_production;
    private TextInputEditText new_production_main_resource;
    private TextInputEditText new_production_production_amount;
    private TextInputEditText new_production_production_power;
    private Button new_production_create_new_action;
    private RecyclerView new_production_actions;
    private Button new_production_validate_and_create;
    private Button new_production_cancel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        _stateHandler = NewProductionStateHandler.getInstance();
        View root = inflater.inflate(R.layout.new_production_creation, container, false);
        setupAllViewElements(root);
        fillViewElementsWithValues();
        return root;
    }

    private void fillViewElementsWithValues() {
        new_production_name_of_production.setText(_stateHandler.getProdName());
        new_production_main_resource.setText(_stateHandler.getMainResource());
        new_production_production_amount.setText(_stateHandler.getAmount());
        new_production_production_power.setText(_stateHandler.getPower());
        if (!(_stateHandler.getActions() == null || _stateHandler.getActions().isEmpty())) {
            new_production_actions.setAdapter(new ProductionActionAdapter(getContext(),_stateHandler.getActions(),this));
            new_production_actions.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    private void setupAllViewElements(View root) {
        new_production_name_of_production = root.findViewById(R.id.new_production_name_of_production);
        new_production_main_resource = root.findViewById(R.id.new_production_main_resource);
        new_production_production_amount = root.findViewById(R.id.new_production_production_amount);
        new_production_production_power = root.findViewById(R.id.new_production_production_power);
        new_production_create_new_action = root.findViewById(R.id.new_production_create_new_action);
        new_production_create_new_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAllValuesToStateHandler();
                NewProductionActionDialog actionDialog = new NewProductionActionDialog(getContext(), ProductionActionContext.NEW_PRODUCTION,0);
                actionDialog.show();
                actionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        fillViewElementsWithValues();
                    }
                });
            }
        });
        new_production_actions = root.findViewById(R.id.new_production_actions);
        new_production_validate_and_create = root.findViewById(R.id.new_production_validate_and_create);
        new_production_validate_and_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String validationResult = validateInputData();
                if (validationResult.isEmpty() || validationResult.trim() == "") {
                    WineManager service = new WineManager(getContext());
                    service.handleNewProductionCreation(_stateHandler.getProdName(),
                            _stateHandler.getMainResource(),
                            Integer.valueOf(_stateHandler.getAmount()),
                            Integer.valueOf(_stateHandler.getPower()),
                            _stateHandler.getActions());
                    _stateHandler.clearNewProductionStateHandlerData();
                    Intent mainAc = new Intent(getActivity(), MainActivity.class);
                    startActivity(mainAc);
                } else {
                    Snackbar.make(root,validationResult, BaseTransientBottomBar.LENGTH_SHORT).setTextColor(Color.BLACK).show();
                }
            }
        });
        new_production_cancel = root.findViewById(R.id.new_production_cancel);
        new_production_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _stateHandler.clearNewProductionStateHandlerData();
                Intent mainAc = new Intent(getActivity(), MainActivity.class);
                startActivity(mainAc);
            }
        });
    }

    private String validateInputData() {
        StringBuilder sb = new StringBuilder();
        if (_stateHandler.getProdName() == null || _stateHandler.getProdName().isEmpty() || _stateHandler.getProdName().trim().equals("")) {
            sb.append(getContext().getString(R.string.production_validation_name));
            sb.append("\n");
        }
        if (_stateHandler.getMainResource() == null || _stateHandler.getMainResource().isEmpty() || _stateHandler.getMainResource().trim().equals("")) {
            sb.append(getContext().getString(R.string.production_validation_main_resource));
            sb.append("\n");
        }
        try {
            Integer.valueOf(_stateHandler.getAmount());
        } catch (Exception ex) {
            sb.append(getContext().getString(R.string.production_validation_amount));
            sb.append("\n");
        }

        try {
            Integer.valueOf(_stateHandler.getPower());
        } catch (Exception ex) {
            sb.append(getContext().getString(R.string.production_validation_power));
            sb.append("\n");
        }

        if (_stateHandler.getActions() == null || _stateHandler.getActions().isEmpty() || _stateHandler.getActions().size() == 0) {
            sb.append(getContext().getString(R.string.production_validation_actions));
            sb.append("\n");
        }
        return sb.toString();
    }

    private void saveAllValuesToStateHandler() {
        _stateHandler.setAmount(new_production_production_amount.getText().toString());
        _stateHandler.setMainResource(new_production_main_resource.getText().toString());
        _stateHandler.setPower(new_production_production_power.getText().toString());
        _stateHandler.setProdName(new_production_name_of_production.getText().toString());
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onProdActionListItemClick(int position) {}
}