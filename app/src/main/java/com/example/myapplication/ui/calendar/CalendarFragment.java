package com.example.myapplication.ui.calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.CalendarAction;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.CalendarAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.view.View.INVISIBLE;

public class CalendarFragment extends Fragment implements CalendarAdapter.CalendarItemClickListener{
    private RecyclerView calendarActionsRecycleView;
    private ArrayList<CalendarAction> calendarActions;
    private CalendarView calendarView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.calendar_section_layout, container, false);
        View floatingPlus = getActivity().findViewById(R.id.fab);
        floatingPlus.setVisibility(INVISIBLE);
        WineManager manager = new WineManager(getContext());
        calendarActionsRecycleView = root.findViewById(R.id.calendar_actions_recycle_view);

        calendarView = root.findViewById(R.id.calendar_screen_calendar_view);
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year,month,dayOfMonth);
            Date selectedDate = calendar.getTime();
            calendarActions = manager.getActionsForDate(selectedDate);
            CalendarAdapter calendarAdapter = new CalendarAdapter(getContext(), calendarActions,CalendarFragment.this::onCalendarListItemClick);
            calendarActionsRecycleView.setAdapter(calendarAdapter);
            calendarActionsRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        });
        return root;
    }

    @Override
    public void onCalendarListItemClick(int position) {
        Toast.makeText(getContext(), calendarActions.get(position).getProductionName(), Toast.LENGTH_SHORT).show();
    }
}
