package com.example.myapplication.ui.newProductionAction;

import com.example.myapplication.model.ActionStatus;
import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.Ingredient;

import java.util.ArrayList;
import java.util.Date;

public class ProductionActionStateHandler {
    private static ProductionActionStateHandler _instance;
    private ActionType actionType;
    private ActionStatus actionStatus;
    private Date actionDate;
    private ArrayList<Ingredient> ingredients;
    private String comments;

    public static ProductionActionStateHandler getInstance() {
        if (_instance == null) {
            _instance = new ProductionActionStateHandler();
        }
        return _instance;
    }

    public ProductionActionStateHandler() {
        ingredients = new ArrayList<>();
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
    }

    public void addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void clearProdActionData() {
        this.ingredients.clear();
        this.actionDate = null;
        this.actionType = null;
        this.actionStatus = null;
        this.comments = "";
    }
}
