package com.example.myapplication.ui.newProductionAction;

public enum ProductionActionContext {
    NEW_PRODUCTION,
    ADD_TO_EXISTING_PRODUCTION
}
