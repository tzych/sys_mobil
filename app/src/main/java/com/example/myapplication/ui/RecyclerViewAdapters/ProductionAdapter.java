package com.example.myapplication.ui.RecyclerViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Production;

import java.util.ArrayList;

public class ProductionAdapter extends RecyclerView.Adapter<ProductionAdapter.ProductionViewHolder> {
    private Context context;
    private ArrayList<Production> productions;
    final private ListItemClickListener onClickListener;

    public interface ListItemClickListener{
        void onListItemClick(int position);
    }

    public ProductionAdapter(Context context, ArrayList<Production> productions,ListItemClickListener onClickListener) {
        this.context = context;
        this.productions = productions;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ProductionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_item_for_production, parent, false);
        return new ProductionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductionViewHolder holder, int position) {
        Production prod = productions.get(position);
        holder.image.setImageResource(prod.getImage());
        holder.name.setText(prod.getName());
        holder.mainResource.setText(context.getString(R.string.production_item_main_resource) + " " + prod.getMainComponent());
        holder.amount.setText(context.getString(R.string.production_item_amount) + " " + String.valueOf(prod.getAmountOfProduct()) + " " + context.getString(R.string.production_item_amount_unit));
        holder.power.setText(context.getString(R.string.production_item_power) + " " + String.valueOf(prod.getYeastPower()) + context.getString(R.string.production_item_power_unit));
        holder.sugarNeeded.setText(context.getString(R.string.production_item_sugar_needed) + " " + String.valueOf(prod.sugarNeededForYeast()) + context.getString(R.string.production_item_sugar_unit));
        holder.sugar.setText(context.getString(R.string.production_item_sugar_already_added) + " " + String.valueOf(prod.sugarInProductionAlready()) + context.getString(R.string.production_item_sugar_unit));
        holder.startDate.setText(context.getString(R.string.production_item_sugar_production_start) + " " + String.valueOf(prod.getStartDateOfProduction()));

    }

    @Override
    public int getItemCount() {
        return productions.size();
    }

    public class ProductionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView image;
        TextView name, mainResource, amount, power, sugarNeeded, sugar, startDate;

        public ProductionViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.prod_detail_image);
            name = itemView.findViewById(R.id.prod_detail_name);
            mainResource = itemView.findViewById(R.id.prod_detail_main_resource);
            amount = itemView.findViewById(R.id.prod_detail_amount);
            power = itemView.findViewById(R.id.prod_detail_power);
            sugarNeeded = itemView.findViewById(R.id.prod_detail_sugar_needed);
            sugar = itemView.findViewById(R.id.prod_detail_sugar_already_in);
            startDate = itemView.findViewById(R.id.prod_detail_start_date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            onClickListener.onListItemClick(position);
        }
    }
}
