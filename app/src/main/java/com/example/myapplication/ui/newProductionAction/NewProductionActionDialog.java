package com.example.myapplication.ui.newProductionAction;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ActionStatus;
import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.IngredientAdapter;
import com.example.myapplication.ui.newProduction.NewProductionStateHandler;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;

public class NewProductionActionDialog extends Dialog{
    private Spinner popup_new_action_action_type_spinner;
    private Spinner popup_new_action_action_status_spinner;
    private CalendarView popup_new_action_action_date;
    private Button popup_new_action_add_ingredient;
    private RecyclerView popup_new_action_ingredients;
    private EditText popup_new_action_comments;
    private Button popup_new_action_create;
    private Button popup_new_action_cancel;
    private ProductionActionStateHandler _stateHandler;
    private ProductionActionContext prodContext;
    private WineManager manager;
    private int prodID;

    public NewProductionActionDialog(@NonNull Context context, ProductionActionContext prodContext, int prodID) {
        super(context);
        this.prodContext = prodContext;
        this.prodID = prodID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = new WineManager(getContext());
        setContentView(R.layout.popup_new_action);
        setCancelable(false);
        _stateHandler = ProductionActionStateHandler.getInstance();
        loadViewElements();
        setupViewElements();
        setupOnClickListeners();
    }

    private void setupOnClickListeners() {
        popup_new_action_action_date.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year,month,dayOfMonth);
            _stateHandler.setActionDate(calendar.getTime());
        });
        popup_new_action_add_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValuesToStateHandler();
                View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.popup_new_ingredient,null, false);
                AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setCancelable(false)
                        .setTitle(getContext().getString(R.string.popup_new_ingredient_title))
                        .setView(viewInflated)
                        .setPositiveButton(android.R.string.ok,null)
                        .setNegativeButton(android.R.string.cancel,null)
                        .show();

                final EditText newIngredientName = viewInflated.findViewById(R.id.popup_add_product_name);
                final EditText newIngredientAmount = viewInflated.findViewById(R.id.popup_add_ingredient_amount);
                final EditText sugarInNewIngredient = viewInflated.findViewById(R.id.popup_add_ingredient_sugar_amount);

                Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String ingName = newIngredientName.getText().toString();
                        String ingAmount = newIngredientAmount.getText().toString();
                        String ingSugarAmount = sugarInNewIngredient.getText().toString();
                        String validationResult = validateNewIngredient(ingName, ingAmount, ingSugarAmount);
                        if (validationResult.isEmpty()) {
                            _stateHandler.addIngredient(new Ingredient(ingName, Integer.valueOf(ingAmount), Integer.valueOf(ingSugarAmount)));
                            alertDialog.dismiss();
                        } else {
                            Snackbar.make(viewInflated, validationResult, BaseTransientBottomBar.LENGTH_SHORT).setTextColor(Color.BLACK).show();
                        }
                        setupViewElements();
                    }
                });

                Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.cancel();
                    }
                });
            }
        });
        popup_new_action_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValuesToStateHandler();
                ProductionAction prodAction = new ProductionAction(
                        _stateHandler.getActionType(),
                        (ArrayList<Ingredient>)_stateHandler.getIngredients().clone(),
                        _stateHandler.getActionDate(),
                        _stateHandler.getComments(),
                        _stateHandler.getActionStatus()
                );
                if(prodContext == ProductionActionContext.NEW_PRODUCTION) {
                    NewProductionStateHandler productionContext = NewProductionStateHandler.getInstance();
                    productionContext.addAction(prodAction);
                } else {
                    manager.addActionToExistingProduction(prodAction ,prodID);
                }
                dismiss();
                clearStateHandlerData();
            }
        });
        popup_new_action_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearStateHandlerData();
                dismiss();
            }
        });
    }

    private void saveValuesToStateHandler() {
        _stateHandler.setActionType((ActionType)popup_new_action_action_type_spinner.getSelectedItem());
        _stateHandler.setActionStatus((ActionStatus) popup_new_action_action_status_spinner.getSelectedItem());
        _stateHandler.setComments(popup_new_action_comments.getText().toString());
    }

    private void clearStateHandlerData() {
        _stateHandler.clearProdActionData();
    }

    private String validateNewIngredient(String name, String amount, String sugarAmount) {
        StringBuilder sb = new StringBuilder();
        if (name == null || name.isEmpty()) {
            sb.append(getContext().getString(R.string.popup_new_ingredient_validation_name));
            sb.append("\n");
        }

        try {
            Integer.valueOf(amount.trim());
        } catch (Exception ex) {
            sb.append(getContext().getString(R.string.popup_new_ingredient_validation_amount));
            sb.append("\n");
        }

        try {
            Integer.valueOf(sugarAmount.trim());
        } catch (Exception ex) {
            sb.append(getContext().getString(R.string.popup_new_ingredient_validation_sugar_amount));
            sb.append("\n");
        }

        return sb.toString();
    }

    private void loadViewElements() {
        popup_new_action_action_type_spinner = findViewById(R.id.popup_new_action_action_type_spinner);
        popup_new_action_action_status_spinner = findViewById(R.id.popup_new_action_action_status_spinner);
        popup_new_action_action_date = findViewById(R.id.popup_new_action_action_date);
        popup_new_action_add_ingredient = findViewById(R.id.popup_new_action_add_ingredient);
        popup_new_action_ingredients = findViewById(R.id.popup_new_action_ingredients);
        popup_new_action_comments = findViewById(R.id.popup_new_action_comments);
        popup_new_action_create = findViewById(R.id.popup_new_action_create);
        popup_new_action_cancel = findViewById(R.id.popup_new_action_cancel);
    }

    private void setupViewElements() {
        ArrayAdapter<ActionType> adapter = new ArrayAdapter<ActionType>(getContext(), android.R.layout.simple_spinner_dropdown_item, ActionType.values());
        popup_new_action_action_type_spinner.setAdapter(adapter);
        ArrayAdapter<ActionStatus> statusAdapter = new ArrayAdapter<ActionStatus>(getContext(), android.R.layout.simple_spinner_dropdown_item, ActionStatus.values());
        popup_new_action_action_status_spinner.setAdapter(statusAdapter);
        if (_stateHandler.getActionType() != null) {
            popup_new_action_action_type_spinner.setSelection(adapter.getPosition(_stateHandler.getActionType()));
        }
        if (_stateHandler.getActionStatus() != null) {
            popup_new_action_action_status_spinner.setSelection(statusAdapter.getPosition(_stateHandler.getActionStatus()));
        }
        if (_stateHandler.getActionDate() != null) {
            popup_new_action_action_date.setDate(_stateHandler.getActionDate().getTime());
        }

        if (!(_stateHandler.getIngredients() == null || _stateHandler.getIngredients().isEmpty())) {
            popup_new_action_ingredients.setAdapter(new IngredientAdapter(getContext(),_stateHandler.getIngredients()));
            popup_new_action_ingredients.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        popup_new_action_comments.setText(_stateHandler.getComments());
    }
}
