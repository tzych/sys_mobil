package com.example.myapplication.ui.RecyclerViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ProductionAction;

import java.util.ArrayList;

public class ProductionActionAdapter extends RecyclerView.Adapter<ProductionActionAdapter.ProductionActionViewHolder> {
    private Context context;
    private ArrayList<ProductionAction> productionActions;
    final private ProductionActionAdapter.ProductionActionClickListener onClickListener;

    public interface ProductionActionClickListener{
        void onProdActionListItemClick(int position);
    }

    public ProductionActionAdapter(Context context, ArrayList<ProductionAction> productionActions, ProductionActionAdapter.ProductionActionClickListener onClickListener) {
        this.context = context;
        this.productionActions = productionActions;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ProductionActionAdapter.ProductionActionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_item_for_production_action, parent, false);
        return new ProductionActionAdapter.ProductionActionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductionActionAdapter.ProductionActionViewHolder holder, int position) {
        ProductionAction prod = productionActions.get(position);
        String actionTypeLabel = context.getString(R.string.production_action_row_type) + prod.getActionType() != null ? prod.getActionType().getDesc() : "";
        holder.actionType.setText(actionTypeLabel);
        String actionDateLabel = context.getString(R.string.production_action_row_date) + prod.getDateOfAction() != null ? prod.getDateOfAction().toString() : "";
        holder.actionDate.setText(actionDateLabel);
        if (!(prod.getIngredientsList() == null || prod.getIngredientsList().isEmpty())) {
            holder.ingredients.setAdapter(new IngredientAdapter(context, prod.getIngredientsList()));
            holder.ingredients.setLayoutManager(new LinearLayoutManager(context));
        }
        holder.comments.setText(context.getString(R.string.production_action_row_comments) + " " + (prod.getAdditionalComments()  == null ? "" : prod.getAdditionalComments()));
        String actionStatusLabel = context.getString(R.string.production_action_row_status) + prod.getActionStatus() != null ? prod.getActionStatus().getDesc() : "";
        holder.actionStatus.setText(actionStatusLabel);
    }

    @Override
    public int getItemCount() {
        return productionActions.size();
    }

    public class ProductionActionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView actionType, actionDate, comments, actionStatus;
        RecyclerView ingredients;

        public ProductionActionViewHolder(@NonNull View itemView) {
            super(itemView);
            actionType = itemView.findViewById(R.id.production_action_type);
            actionDate = itemView.findViewById(R.id.production_action_date);
            comments = itemView.findViewById(R.id.production_action_comments);
            ingredients = itemView.findViewById(R.id.production_action_ingredients);
            actionStatus = itemView.findViewById(R.id.production_action_status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            onClickListener.onProdActionListItemClick(position);
        }
    }
}