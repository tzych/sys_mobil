package com.example.myapplication.service.DAO;

import android.content.Context;

import com.example.myapplication.R;
import com.example.myapplication.model.ActionStatus;
import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.utils.DateHelper;

import java.util.ArrayList;

public class FakeValueProvider {

    public static void loadSampleData(Context context) {
        DatabaseAccessor _dbManager = new DatabaseAccessor(context);
        ArrayList<Production> productions = new ArrayList<>();

        // ToDO: Prod 1
        Production prod = new Production("Super szybki hibiskus","Hibiskus", 18, 25, R.mipmap.wine_1);
        ProductionAction startAction = new ProductionAction(ActionType.START_OF_PRODUCTION, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),-5)),null);
        startAction.addIngredient(new Ingredient("Sugar",7000, 7000));
        startAction.addIngredient(new Ingredient("Hibiscus",600,0));
        startAction.addIngredient(new Ingredient("Rodzynki",1000,600));
        startAction.setActionStatus(ActionStatus.DONE);

        prod.addAction(startAction);

        ProductionAction nextAction = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),10)),null);
        nextAction.addIngredient(new Ingredient("Sugar",1000,1000));
        nextAction.setActionStatus(ActionStatus.TO_DO);
        prod.addAction(nextAction);
        ProductionAction throwingFruits = new ProductionAction(ActionType.REMOVING_FRUITS, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),20)),null);
        throwingFruits.setActionStatus(ActionStatus.TO_DO);
        prod.addAction(throwingFruits);
        productions.add(prod);
        _dbManager.addProduction(prod);

        // ToDO: Prod 2
        Production prod2 = new Production("Zielonka","Zielona Herbata", 14, 25, R.mipmap.wine_2);
        ProductionAction startAction2 = new ProductionAction(ActionType.START_OF_PRODUCTION, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),-10)),null);
        startAction2.addIngredient(new Ingredient("Sugar",4000, 4000));
        startAction2.addIngredient(new Ingredient("Zielona Herbata",400,0));
        startAction2.addIngredient(new Ingredient("Rodzynki",1000,600));
        startAction2.addIngredient(new Ingredient("Żurawina",1000,400));
        startAction2.setActionStatus(ActionStatus.DONE);

        prod2.addAction(startAction2);

        ProductionAction nextAction2 = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),11)),null);
        nextAction2.addIngredient(new Ingredient("Sugar",1300,1300));
        nextAction2.setActionStatus(ActionStatus.TO_DO);
        prod2.addAction(nextAction2);
        ProductionAction throwingFruits2 = new ProductionAction(ActionType.REMOVING_FRUITS, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),30)),null);
        throwingFruits2.setActionStatus(ActionStatus.TO_DO);
        prod2.addAction(throwingFruits2);
        productions.add(prod2);
        _dbManager.addProduction(prod2);

        // ToDO: Prod 3
        Production prod3 = new Production("Malina Browin","Malina", 17, 10, R.mipmap.wine_3);
        ProductionAction startAction3 = new ProductionAction(ActionType.START_OF_PRODUCTION, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),-11)),null);
        startAction3.addIngredient(new Ingredient("Sugar",2000, 2000));
        startAction3.addIngredient(new Ingredient("Malina Browin",100,0));
        startAction3.setActionStatus(ActionStatus.DONE);
        prod3.addAction(startAction3);

        ProductionAction nextAction3 = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),5)),null);
        nextAction3.addIngredient(new Ingredient("Sugar",800,800));
        nextAction3.setActionStatus(ActionStatus.TO_DO);
        prod3.addAction(nextAction3);
        ProductionAction throwingFruits3 = new ProductionAction(ActionType.REMOVING_FRUITS, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),15)),null);
        throwingFruits3.setActionStatus(ActionStatus.TO_DO);
        prod3.addAction(throwingFruits3);
        productions.add(prod3);
        _dbManager.addProduction(prod3);

        // ToDO: Prod 4
        Production prod4 = new Production("Wiśnia owoc","Wiśnia", 18, 20, R.mipmap.wine_4);
        ProductionAction startAction4 = new ProductionAction(ActionType.START_OF_PRODUCTION, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),-20)),null);
        startAction4.setActionStatus(ActionStatus.DONE);
        startAction4.addIngredient(new Ingredient("Sugar",2000, 2000));
        startAction4.addIngredient(new Ingredient("Wiśnia",15000, 1800));
        prod4.addAction(startAction4);


        ProductionAction nextAction4 = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),5)),null);
        nextAction4.addIngredient(new Ingredient("Sugar",800,800));
        nextAction4.setActionStatus(ActionStatus.TO_DO);
        prod4.addAction(nextAction4);
        ProductionAction throwingFruits4 = new ProductionAction(ActionType.REMOVING_FRUITS, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),15)),null);
        prod4.addAction(throwingFruits4);
        throwingFruits4.setActionStatus(ActionStatus.TO_DO);
        productions.add(prod4);
        _dbManager.addProduction(prod4);
        // ToDO: Prod 5
        Production prod5 = new Production("Hibi MOC","Hibiskus", 20, 27, R.mipmap.wine_5);
        ProductionAction startAction5 = new ProductionAction(ActionType.START_OF_PRODUCTION, DateHelper.changeDateFormat(DateHelper.currentDate()),null);
        startAction5.addIngredient(new Ingredient("Sugar",6000, 6000));
        startAction5.addIngredient(new Ingredient("Hibiscus",600,0));
        startAction5.addIngredient(new Ingredient("Rodzynki",1000,600));
        startAction5.setActionStatus(ActionStatus.DONE);
        prod5.addAction(startAction5);

        ProductionAction nextAction5 = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),5)),null);
        nextAction5.addIngredient(new Ingredient("Sugar",2000,2000));
        nextAction5.setActionStatus(ActionStatus.TO_DO);
        prod5.addAction(nextAction5);
        ProductionAction throwingFruits5 = new ProductionAction(ActionType.REMOVING_FRUITS, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),15)),null);
        throwingFruits5.setActionStatus(ActionStatus.TO_DO);
        prod5.addAction(throwingFruits5);
        ProductionAction addingSugar2 = new ProductionAction(ActionType.ADDING_SUGAR, DateHelper.changeDateFormat(DateHelper.addDays(DateHelper.currentDate(),30)),null);
        addingSugar2.addIngredient(new Ingredient("Sugar",2500,2500));
        addingSugar2.setActionStatus(ActionStatus.TO_DO);
        prod5.addAction(addingSugar2);
        productions.add(prod5);
        _dbManager.addProduction(prod5);

        // PRODUCTS
        _dbManager.addProduct("Malina", "Wino malinowe stworzone z torebki z b****** (lokowanie produktu)", 30);
        _dbManager.addProduct("Hibiskus", "Wino Hibiskusowe na drożdżach turbo", 20);
        _dbManager.addProduct("Zielona Herbata", "Zielona Herbata", 20);
        _dbManager.addProduct("Malina", "Wino malinowe stworzone z torebki z b****** (lokowanie produktu)", 30);
        _dbManager.addProduct("Hibiskus", "Wino Hibiskusowe na drożdżach turbo", 20);
        _dbManager.addProduct("Zielona Herbata", "Zielona Herbata", 20);
        _dbManager.addProduct("Malina", "Wino malinowe stworzone z torebki z b****** (lokowanie produktu)", 30);
        _dbManager.addProduct("Hibiskus", "Wino Hibiskusowe na drożdżach turbo", 20);
        _dbManager.addProduct("Zielona Herbata", "Zielona Herbata", 20);
    }
}
