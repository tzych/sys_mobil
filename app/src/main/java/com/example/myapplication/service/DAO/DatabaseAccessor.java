package com.example.myapplication.service.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.myapplication.model.ActionStatus;
import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.model.Product;
import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;

import java.util.ArrayList;
import java.util.Date;

public class DatabaseAccessor extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "wine_manager";
    private static final int DATABASE_VERSION = 14;
    private static final String TABLE_INGREDIENT = "ingredients";
    private static final String TABLE_PRODUCTION_ACTION = "production_actions";
    private static final String TABLE_PRODUCTION = "productions";
    private static final String TABLE_PRODUCTS = "products";

    //Commons
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    //Ingredient specific
    private static final String KEY_PRODUCTION_ACTION_ID = "actionID";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_SUGAR_AMOUNT = "sugarAmount";

    // Production Action Specific
    private static final String KEY_PRODUCTION_ID = "productionID";
    private static final String KEY_ACTION_TYPE = "actionType";
    private static final String KEY_DATE_OF_ACTION = "actionDate";
    private static final String KEY_COMMENTS = "comments";
    private static final String KEY_ACTION_STATUS = "actionStatus";

    // Production specific
    private static final String KEY_MAIN_COMPONENT = "mainComponent";
    private static final String KEY_YEAST_POWER = "yeastPower";
    private static final String KEY_PRODUCTION_AMOUNT = "amount";
    private static final String KEY_IMAGE = "image";

    // Products specific
    private static final String KEY_DESCRIPTION = "description";


    private static final String CREATE_TABLE_INGREDIENTS = "CREATE TABLE " + TABLE_INGREDIENT + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_PRODUCTION_ACTION_ID + " INTEGER, "
            + KEY_NAME + " TEXT, "
            + KEY_AMOUNT + " INTEGER, "
            + KEY_SUGAR_AMOUNT + " INTEGER);";

    private static final String CREATE_TABLE_PRODUCTION_ACTIONS = "CREATE TABLE " + TABLE_PRODUCTION_ACTION + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_PRODUCTION_ID + " INTEGER, "
            + KEY_ACTION_TYPE + " TEXT, "
            + KEY_DATE_OF_ACTION + " TEXT, "
            + KEY_COMMENTS + " TEXT, "
            + KEY_ACTION_STATUS + " TEXT);";

    private static final String CREATE_TABLE_PRODUCTIONS = "CREATE TABLE " + TABLE_PRODUCTION + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_NAME + " TEXT, "
            + KEY_MAIN_COMPONENT + " TEXT, "
            + KEY_YEAST_POWER + " INTEGER, "
            + KEY_PRODUCTION_AMOUNT + " INTEGER, "
            + KEY_IMAGE + " INTEGER); ";

    private static final String CREATE_TABLE_PRODUCTS = "CREATE TABLE " + TABLE_PRODUCTS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_NAME + " TEXT, "
            + KEY_DESCRIPTION + " TEXT, "
            + KEY_AMOUNT + " INTEGER); ";

    public DatabaseAccessor(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_INGREDIENTS);
        db.execSQL(CREATE_TABLE_PRODUCTION_ACTIONS);
        db.execSQL(CREATE_TABLE_PRODUCTIONS);
        db.execSQL(CREATE_TABLE_PRODUCTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_INGREDIENT + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_PRODUCTION_ACTION + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_PRODUCTION + "'");
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_PRODUCTS + "'");
        onCreate(db);
    }

    public void addIngredient(int productionActionID, String name, int amount, int sugarAmount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCTION_ACTION_ID, productionActionID);
        values.put(KEY_NAME, name);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_SUGAR_AMOUNT, sugarAmount);
        long id = db.insertWithOnConflict(TABLE_INGREDIENT, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public ArrayList<Ingredient> getAllIngredientsForAction(int productionActionId) {
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_INGREDIENT + " WHERE " + KEY_PRODUCTION_ACTION_ID + " LIKE " + productionActionId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Ingredient ingredient = new Ingredient(
                        c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getInt(c.getColumnIndex(KEY_PRODUCTION_ACTION_ID)),
                        c.getString(c.getColumnIndex(KEY_NAME)),
                        c.getInt(c.getColumnIndex(KEY_AMOUNT)),
                        c.getInt(c.getColumnIndex(KEY_SUGAR_AMOUNT)));

                ingredients.add(ingredient);
            } while (c.moveToNext());
        }
        return ingredients;
    }

    public void updateIngredient(int id, String name, int amount, int sugarAmount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_SUGAR_AMOUNT, sugarAmount);
        db.update(TABLE_INGREDIENT, values, KEY_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public void updateIngredient(Ingredient ingredient) {
        updateIngredient(ingredient.getId(),
                ingredient.getName(),
                ingredient.getAmount(),
                ingredient.getSugarAmount());
    }

    public void deleteIngredient(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INGREDIENT, KEY_ID + " = ?",new String[]{String.valueOf(id)});
    }

    public void addProductionAction(int productionID, ProductionAction productionAction) {
        addProductionAction(productionID,
                productionAction.getActionType(),
                productionAction.getDateOfAction(),
                productionAction.getActionStatus(),
                productionAction.getIngredientsList());
    }

    public void addProductionAction(int productionID, ActionType actionType, Date dateOfAction, ActionStatus actionStatus, ArrayList<Ingredient> ingredients) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCTION_ID, productionID);
        values.put(KEY_ACTION_TYPE, actionType.toString());
        values.put(KEY_DATE_OF_ACTION, String.valueOf(dateOfAction.getTime()));
        values.put(KEY_ACTION_STATUS, actionStatus.toString());
        long id = db.insertWithOnConflict(TABLE_PRODUCTION_ACTION, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        for(Ingredient ing : ingredients) {
            addIngredient((int) id,ing.getName(),ing.getAmount(),ing.getSugarAmount());
        }
    }

    public ArrayList<ProductionAction> getAllProductionActionsForProduction(int productionId) {
        ArrayList<ProductionAction> productionActions = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTION_ACTION + " WHERE " + KEY_PRODUCTION_ID + " = " + productionId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                ProductionAction productionAction = new ProductionAction(
                        c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getInt(c.getColumnIndex(KEY_PRODUCTION_ID)),
                        ActionType.valueOf(c.getString(c.getColumnIndex(KEY_ACTION_TYPE))),
                        new Date(c.getLong(c.getColumnIndex(KEY_DATE_OF_ACTION))),
                        c.getString(c.getColumnIndex(KEY_COMMENTS)),
                        ActionStatus.valueOf(c.getString(c.getColumnIndex(KEY_ACTION_STATUS))));
                productionAction.setIngredientsList(getAllIngredientsForAction(productionAction.getId()));
                productionActions.add(productionAction);
            } while (c.moveToNext());
        }
        return productionActions;
    }

    public ProductionAction getProductionActionById(int productionActionId) {
        ProductionAction productionAction = null;
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCTION_ACTION + " WHERE " + KEY_ID + " = " + productionActionId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
                productionAction = new ProductionAction(
                        c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getInt(c.getColumnIndex(KEY_PRODUCTION_ID)),
                        ActionType.valueOf(c.getString(c.getColumnIndex(KEY_ACTION_TYPE))),
                        new Date(c.getLong(c.getColumnIndex(KEY_DATE_OF_ACTION))),
                        c.getString(c.getColumnIndex(KEY_COMMENTS)),
                        ActionStatus.valueOf(c.getString(c.getColumnIndex(KEY_ACTION_STATUS))));
                productionAction.setIngredientsList(getAllIngredientsForAction(productionAction.getId()));
        }
        return productionAction;
    }

    public void updateProductionAction(int id, ActionType actionType, Date dateOfAction, ActionStatus actionStatus, ArrayList<Ingredient> ingredients) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACTION_TYPE, actionType.toString());
        values.put(KEY_DATE_OF_ACTION, String.valueOf(dateOfAction.getTime()));
        values.put(KEY_ACTION_STATUS, actionStatus.toString());
        db.update(TABLE_PRODUCTION_ACTION, values, KEY_ID + " = ?", new String[]{String.valueOf(id)});
        for(Ingredient ingredient : ingredients) {
            updateIngredient(ingredient);
        }
    }
    public void updateProductionAction(ProductionAction action) {
        updateProductionAction(action.getId(),
                action.getActionType(),
                action.getDateOfAction(),
                action.getActionStatus(),
                action.getIngredientsList());
    }

    public void deleteProductionAction(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ProductionAction prod = getProductionActionById(id);
        deleteProductionAction(prod);
    }

    public void deleteProductionAction(ProductionAction productionAction) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (Ingredient ingredient : productionAction.getIngredientsList()) {
            deleteIngredient(ingredient.getId());
        }
        db.delete(TABLE_PRODUCTION_ACTION, KEY_ID + " = ?",new String[]{String.valueOf(productionAction.getId())});
    }

    public void addProduction(String name, String mainComponent, int yeastPower, int amountOfProduct, int image, ArrayList<ProductionAction> actions) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_MAIN_COMPONENT, mainComponent);
        values.put(KEY_YEAST_POWER, yeastPower);
        values.put(KEY_AMOUNT, amountOfProduct);
        values.put(KEY_IMAGE, image);
        long id = db.insertWithOnConflict(TABLE_PRODUCTION, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        for(ProductionAction act : actions) {
            addProductionAction((int) id,act.getActionType(),act.getDateOfAction(),act.getActionStatus(),act.getIngredientsList());
        }
    }

    public void addProduction(Production production) {
        addProduction(production.getName(),
                production.getMainComponent(),
                production.getYeastPower(),
                production.getAmountOfProduct(),
                production.getImage(),
                production.getActions());
    }

    public ArrayList<Production> getAllProductions() {
        ArrayList<Production> productions = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Production production = new Production(
                        c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getString(c.getColumnIndex(KEY_NAME)),
                        c.getString(c.getColumnIndex(KEY_MAIN_COMPONENT)),
                        c.getInt(c.getColumnIndex(KEY_YEAST_POWER)),
                        c.getInt(c.getColumnIndex(KEY_PRODUCTION_AMOUNT)),
                        c.getInt(c.getColumnIndex(KEY_IMAGE)));
                production.setActions(getAllProductionActionsForProduction(production.getId()));
                productions.add(production);
            } while (c.moveToNext());
        }
        return productions;
    }

    public Production getProductionWithID(int productionId) {
        Production production = null;
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCTION + " WHERE " + KEY_ID + " = " + productionId;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            production = new Production(
                    c.getInt(c.getColumnIndex(KEY_ID)),
                    c.getString(c.getColumnIndex(KEY_NAME)),
                    c.getString(c.getColumnIndex(KEY_MAIN_COMPONENT)),
                    c.getInt(c.getColumnIndex(KEY_YEAST_POWER)),
                    c.getInt(c.getColumnIndex(KEY_PRODUCTION_AMOUNT)),
                    c.getInt(c.getColumnIndex(KEY_IMAGE)));
                production.setActions(getAllProductionActionsForProduction(production.getId()));
        }
        return production;
    }

    public void updateProduction(int id, String name, String mainComponent, int yeastPower, int amountOfProduct, int image, ArrayList<ProductionAction> actions) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_MAIN_COMPONENT, mainComponent);
        values.put(KEY_YEAST_POWER, yeastPower);
        values.put(KEY_PRODUCTION_AMOUNT, amountOfProduct);
        values.put(KEY_IMAGE, image);
        db.update(TABLE_PRODUCTION, values, KEY_ID + " = ?", new String[]{String.valueOf(id)});
        for(ProductionAction action : actions) {
            updateProductionAction(action);
        }
    }

    public void deleteProduction(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Production prod = getProductionWithID(id);
        for (ProductionAction action : prod.getActions()) {
            deleteProductionAction(action);
        }
        db.delete(TABLE_PRODUCTION, KEY_ID + " = ?",new String[]{String.valueOf(id)});
    }

    public void updateProductionActionStatus(int productionAction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ACTION_STATUS, ActionStatus.DONE.toString());
        db.update(TABLE_PRODUCTION_ACTION, values, KEY_ID + " = ?", new String[]{String.valueOf(productionAction)});
    }

    public void addProduct(String name, String description, int amount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_AMOUNT, amount);
        db.insertWithOnConflict(TABLE_PRODUCTS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    public ArrayList<Product> getAllProducts() {
        ArrayList<Product> products = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Product product = new Product(
                        c.getInt(c.getColumnIndex(KEY_ID)),
                        c.getString(c.getColumnIndex(KEY_NAME)),
                        c.getString(c.getColumnIndex(KEY_DESCRIPTION)),
                        c.getInt(c.getColumnIndex(KEY_AMOUNT)));

                products.add(product);
            } while (c.moveToNext());
        }
        return products;
    }

    public void updateProduct(int id, String name, String description, int amount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_AMOUNT, amount);
        db.update(TABLE_PRODUCTS, values, KEY_ID + " = ?", new String[]{String.valueOf(id)});
    }

    public void updateProduct(Product product) {
        updateProduct(product.getId(),
                product.getName(),
                product.getDescription(),
                product.getAmount());
    }

    public void deleteProduct(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCTS, KEY_ID + " = ?", new String[]{String.valueOf(id)});
    }
}
