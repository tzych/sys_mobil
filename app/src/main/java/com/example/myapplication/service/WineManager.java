package com.example.myapplication.service;

import android.content.Context;
import android.os.Build;

import com.example.myapplication.R;
import com.example.myapplication.model.ActionStatus;
import com.example.myapplication.model.ActionType;
import com.example.myapplication.model.CalendarAction;
import com.example.myapplication.model.Ingredient;
import com.example.myapplication.model.Product;
import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.service.DAO.DatabaseAccessor;
import com.example.myapplication.utils.DateHelper;

import java.lang.reflect.GenericArrayType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class WineManager {
    private DatabaseAccessor _dbManager;
    private Context _context;

    public WineManager(Context context) {
        _dbManager = new DatabaseAccessor(context);
        _context = context;
    }

    public ArrayList<Production> getMyProductions() {
            return _dbManager.getAllProductions();
    }

    public ArrayList<Production> getMySortedProductions() {
        ArrayList<Production> prods = getMyProductions();
        Collections.sort(prods, (o1, o2) -> o2.getStartDateOfProduction().compareTo(o1.getStartDateOfProduction()));
        return prods;
    }

    public Production getLastProduction() {
        return  getMySortedProductions().get(0);
    }

    public ArrayList<CalendarAction> getFutureActions() {
        ArrayList<Production> prods = getMyProductions();
        ArrayList<CalendarAction> futureCalendarActions = new ArrayList<>();
        for (Production prod : prods) {
            for (ProductionAction action : prod.getActions()) {
                if (action.getActionStatus() != ActionStatus.DONE) {
                    futureCalendarActions.add(new CalendarAction(action.getId(),prod.getName(), prod.getImage(), action.getActionType(), action.getDateOfAction()));
                }
            }
        }
        Collections.sort(futureCalendarActions, (o1, o2) -> o1.getActionDate().compareTo(o2.getActionDate()));
        return futureCalendarActions;
    }

    private ArrayList<ProductionAction> getAllActions() {
        ArrayList<Production> prods = getMyProductions();
        ArrayList<ProductionAction> allActions = new ArrayList<>();
        for (Production prod : prods) {
            allActions.addAll(prod.getActions());
        }
        return allActions;
    }

    public ArrayList<CalendarAction> getActionsForDate(Date actionDate) {
        ArrayList<Production> prods = getMyProductions();
        actionDate = DateHelper.changeDateFormat(actionDate);
        ArrayList<CalendarAction> actionsForDate = new ArrayList<>();
        for (Production prod : prods) {
            for (ProductionAction action : prod.getActions()) {
                if (action.getDateOfAction().equals(actionDate)) {
                    actionsForDate.add(new CalendarAction(action.getId(),prod.getName(), prod.getImage(), action.getActionType(), action.getDateOfAction()));
                }
            }
        }
        return actionsForDate;
    }

    public Production getProductionWithID(int id) {
        return _dbManager.getProductionWithID(id);
    }

    public void handleNewProductionCreation(String prodName, String mainResource, int amount, int power, ArrayList<ProductionAction> actions) {
        //ToDo :: Fix image..
        _dbManager.addProduction(prodName,mainResource,power,amount,R.mipmap.wine_1,actions);
    }

    public void addActionToExistingProduction(ProductionAction action, int productionID) {
        _dbManager.addProductionAction(productionID,action);
    }

    public void deleteProductionAction(ProductionAction action) {
        _dbManager.deleteProductionAction(action);
    }

    public void deleteCalendarProductionAction(int action) {
        _dbManager.deleteProductionAction(action);
    }

    public ProductionAction getProductionActionThroughID(int prodActionID) {
        return _dbManager.getProductionActionById(prodActionID);
    }

    public void updateProducionAction(int id, ActionType type, Date date, ActionStatus status,  ArrayList<Ingredient> ingredients) {
        _dbManager.updateProductionAction(id,type,date,status,ingredients);
    }

    public void updateProductionActionToDone(int productionAction) {
        _dbManager.updateProductionActionStatus(productionAction);
    }

    public void addNewProduct(String name, String description, int amount) {
        _dbManager.addProduct(name,description,amount);
    }

    public void updateProduct(Product product) {
        _dbManager.updateProduct(product);
    }

    public void deleteProduct(int productID) {
        _dbManager.deleteProduct(productID);
    }

    public ArrayList<Product> getAllProducts() {
        return _dbManager.getAllProducts();
    }
}
