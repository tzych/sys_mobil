package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.model.Production;
import com.example.myapplication.model.ProductionAction;
import com.example.myapplication.service.WineManager;
import com.example.myapplication.ui.RecyclerViewAdapters.ProductionActionAdapter;
import com.example.myapplication.ui.newProductionAction.NewProductionActionDialog;
import com.example.myapplication.ui.newProductionAction.ProductionActionContext;

public class ProductionDetails extends AppCompatActivity implements ProductionActionAdapter.ProductionActionClickListener {
    private ImageView prod_detail_image;
    private TextView prod_detail_name,prod_detail_main_resource,
            prod_detail_amount,prod_detail_power,prod_detail_sugar_needed,
            prod_detail_sugar_already_in,prod_detail_start_date;
    private Button prod_detail_new_action,prod_detail_back;
    private RecyclerView prod_detail_actions;
    private Production _production;
    private Context _context;
    private WineManager manager;
    private int productionID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_details);
        _context = getApplicationContext();
        productionID = getIntent().getExtras().getInt("ProductionID");
        manager = new WineManager(_context);
        setupViewElemenets();
        fillViewElementsWithValues();
        setOnClickListeners();
    }

    private void setupViewElemenets() {
        prod_detail_image = findViewById(R.id.prod_detail_image);
        prod_detail_name = findViewById(R.id.prod_detail_name);
        prod_detail_main_resource = findViewById(R.id.prod_detail_main_resource);
        prod_detail_amount = findViewById(R.id.prod_detail_amount);
        prod_detail_power = findViewById(R.id.prod_detail_power);
        prod_detail_sugar_needed = findViewById(R.id.prod_detail_sugar_needed);
        prod_detail_sugar_already_in = findViewById(R.id.prod_detail_sugar_already_in);
        prod_detail_start_date = findViewById(R.id.prod_detail_start_date);
        prod_detail_new_action = findViewById(R.id.prod_detail_new_action);
        prod_detail_back = findViewById(R.id.prod_detail_back);
        prod_detail_actions = findViewById(R.id.prod_detail_actions);
    }

    private void fillViewElementsWithValues() {
        _production = manager.getProductionWithID(productionID);
        prod_detail_image.setImageResource(_production.getImage());
        prod_detail_name.setText(_production.getName());
        prod_detail_main_resource.setText(_context.getString(R.string.production_item_main_resource) + " " + _production.getMainComponent());
        prod_detail_amount.setText(_context.getString(R.string.production_item_amount) + " " + String.valueOf(_production.getAmountOfProduct()) + " " + _context.getString(R.string.production_item_amount_unit));
        prod_detail_power.setText(_context.getString(R.string.production_item_power) + " " + String.valueOf(_production.getYeastPower()) + _context.getString(R.string.production_item_power_unit));
        prod_detail_sugar_needed.setText(_context.getString(R.string.production_item_sugar_needed) + " " + String.valueOf(_production.sugarNeededForYeast()) + _context.getString(R.string.production_item_sugar_unit));
        prod_detail_sugar_already_in.setText(_context.getString(R.string.production_item_sugar_already_added) + " " + String.valueOf(_production.sugarInProductionAlready()) + _context.getString(R.string.production_item_sugar_unit));
        prod_detail_start_date.setText(_context.getString(R.string.production_item_sugar_production_start) + " " + String.valueOf(_production.getStartDateOfProduction()));
        prod_detail_actions.setAdapter(new ProductionActionAdapter(_context,_production.getActions(),this));
        prod_detail_actions.setLayoutManager(new LinearLayoutManager(_context));
    }

    private void setOnClickListeners() {
        prod_detail_new_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewProductionActionDialog actionDialog = new NewProductionActionDialog(ProductionDetails.this, ProductionActionContext.ADD_TO_EXISTING_PRODUCTION, productionID);
                actionDialog.show();
                actionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        fillViewElementsWithValues();
                    }
                });
            }
        });
        prod_detail_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backToMainActivity = new Intent(_context,MainActivity.class);
                startActivity(backToMainActivity);
            }
        });
    }

    @Override
    public void onProdActionListItemClick(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        ProductionAction selected = _production.getActions().get(position);
        alertDialog.setTitle("Action : " + selected.getActionType().getDesc());
        alertDialog.setMessage("Select what to do with action");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                manager.deleteProductionAction(selected);
                alertDialog.dismiss();
            } });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Go back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }});

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Finish", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                manager.updateProductionActionToDone(selected.getId());
                dialog.dismiss();
            }});

        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fillViewElementsWithValues();
            }
        });
    }
}