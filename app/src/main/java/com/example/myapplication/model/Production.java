package com.example.myapplication.model;

import com.example.myapplication.utils.CONSTANTS;

import java.util.ArrayList;
import java.util.Date;

public class Production {
    private int id;
    private String name;
    private String mainComponent;
    private int yeastPower;
    private int amountOfProduct;
    private int image;
    private ArrayList<ProductionAction> actions;

    public Production(String name, String mainComponent, int yeastPower, int amountOfProduct) {
        this.name = name;
        this.mainComponent = mainComponent;
        this.yeastPower = yeastPower;
        this.amountOfProduct = amountOfProduct;
        this.actions = new ArrayList<ProductionAction>();
    }

    public Production(String name, String mainComponent, int yeastPower, int amountOfProduct, int image) {
        this.name = name;
        this.mainComponent = mainComponent;
        this.yeastPower = yeastPower;
        this.amountOfProduct = amountOfProduct;
        this.image = image;
        this.actions = new ArrayList<ProductionAction>();
    }

    public Production(String name, String mainComponent, int yeastPower, int amountOfProduct, int image, ArrayList<ProductionAction> actions) {
        this.name = name;
        this.mainComponent = mainComponent;
        this.yeastPower = yeastPower;
        this.amountOfProduct = amountOfProduct;
        this.image = image;
        this.actions = actions;
    }

    public Production(int id, String name, String mainComponent, int yeastPower, int amountOfProduct, int image) {
        this.id = id;
        this.name = name;
        this.mainComponent = mainComponent;
        this.yeastPower = yeastPower;
        this.amountOfProduct = amountOfProduct;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainComponent() {
        return mainComponent;
    }

    public void setMainComponent(String mainComponent) {
        this.mainComponent = mainComponent;
    }

    public int getYeastPower() {
        return yeastPower;
    }

    public void setYeastPower(int yeastPower) {
        this.yeastPower = yeastPower;
    }

    public int getAmountOfProduct() {
        return amountOfProduct;
    }

    public void setAmountOfProduct(int amountOfProduct) {
        this.amountOfProduct = amountOfProduct;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public ArrayList<ProductionAction> getActions() {
        return actions;
    }

    public void setActions(ArrayList<ProductionAction> actions) {
        this.actions = actions;
    }

    public void addAction(ProductionAction action) {
        actions.add(action);
    }

    public double sugarNeededForYeast() {
        return Math.round(CONSTANTS.SUGAR_FOR_ONE_PERCENT_ALCOHOL_IN_ONE_LITER * yeastPower * amountOfProduct);
    }

    public double sugarInProductionAlready() {
        double sugarInProduction = 0.0;
        for (ProductionAction action : actions) {
            if (action.getActionStatus() == ActionStatus.DONE) {
                sugarInProduction += action.sugarAddedInAction();
            }
        }
        return sugarInProduction;
    }

    public Date getStartDateOfProduction() {
        for (ProductionAction action : actions) {
            if (action.getActionType() == ActionType.START_OF_PRODUCTION) {
                return action.getDateOfAction();
            }
        }
        return null;
    }
}
