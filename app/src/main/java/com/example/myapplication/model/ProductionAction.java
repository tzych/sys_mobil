package com.example.myapplication.model;

import com.example.myapplication.utils.DateHelper;

import java.util.ArrayList;
import java.util.Date;

public class ProductionAction {
    private int id;
    private int productionID;
    private ActionType actionType;
    private ArrayList<Ingredient> ingredientsList;
    private Date dateOfAction;
    private String additionalComments;
    private ActionStatus actionStatus;

    public ProductionAction(ActionType actionType, Date dateOfAction, String additionalComments) {
        this.actionType = actionType;
        this.dateOfAction = dateOfAction;
        ingredientsList = new ArrayList<Ingredient>();
        this.additionalComments = additionalComments;
    }

    public ProductionAction(ActionType actionType, ArrayList<Ingredient> ingredients, String comments) {
        this.actionType = actionType;
        this.dateOfAction = DateHelper.currentDate();
        this.ingredientsList = ingredients;
        this.additionalComments = comments;
    }

    public ProductionAction(ActionType actionType, ArrayList<Ingredient> ingredients,Date dateOfAction, String comments, ActionStatus actionStatus) {
        this.actionType = actionType;
        this.dateOfAction = DateHelper.currentDate();
        this.ingredientsList = ingredients;
        this.additionalComments = comments;
        this.dateOfAction = dateOfAction;
        this.actionStatus = actionStatus;
    }

    public ProductionAction(int id, int productionID , ActionType actionType, Date dateOfAction, String additionalComments, ActionStatus actionStatus) {
        this.id = id;
        this.productionID = productionID;
        this.actionType = actionType;
        this.dateOfAction = dateOfAction;
        this.additionalComments = additionalComments;
        this.actionStatus = actionStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductionID() {
        return productionID;
    }

    public void setProductionID(int productionID) {
        this.productionID = productionID;
    }

    public void setIngredientsList(ArrayList<Ingredient> ingredientsList) {
        this.ingredientsList = ingredientsList;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public ArrayList<Ingredient> getIngredientsList() {
        return ingredientsList;
    }

    public void addIngredient(Ingredient ingredient) {
       ingredientsList.add(ingredient);
    }

    public Date getDateOfAction() {
        return dateOfAction;
    }

    public void setDateOfAction(Date dateOfAction) {
        this.dateOfAction = dateOfAction;
    }

    public String getAdditionalComments() {
        return additionalComments;
    }

    public void setAdditionalComments(String additionalComments) {
        this.additionalComments = additionalComments;
    }

    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
    }

    public double sugarAddedInAction() {
        double sugar = 0.0;
        for(Ingredient ingredient : ingredientsList) {
            sugar += ingredient.getSugarAmount();
        }
        return sugar;
    }
}
