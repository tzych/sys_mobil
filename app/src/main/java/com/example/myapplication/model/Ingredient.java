package com.example.myapplication.model;

public class Ingredient {
    private int id;
    private int productionActionID;
    private String name;
    private int sugarAmount;
    private int amount;

    public Ingredient(String name, int amount, int sugarAmount) {
        this.name = name;
        this.sugarAmount = sugarAmount;
        this.amount = amount;
    }

    public Ingredient(int id, int productionActionID, String name, int sugarAmount, int amount) {
        this.id = id;
        this.productionActionID = productionActionID;
        this.name = name;
        this.sugarAmount = sugarAmount;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductionActionID() {
        return productionActionID;
    }

    public void setProductionActionID(int productionActionID) {
        this.productionActionID = productionActionID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSugarAmount() {
        return sugarAmount;
    }

    public void setSugarAmount(int sugarAmount) {
        this.sugarAmount = sugarAmount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
