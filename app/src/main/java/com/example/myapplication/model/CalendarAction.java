package com.example.myapplication.model;

import java.util.Date;

public class CalendarAction {
    private int productionActionId;
    private String productionName;
    private int productionImage;
    private ActionType actionType;
    private Date actionDate;

    public CalendarAction(int productionActionId, String productionName, int productionImage, ActionType actionType, Date actionDate) {
        this.productionActionId = productionActionId;
        this.productionName = productionName;
        this.productionImage = productionImage;
        this.actionType = actionType;
        this.actionDate = actionDate;
    }

    public int getProductionActionId() {
        return productionActionId;
    }

    public void setProductionActionId(int productionActionId) {
        this.productionActionId = productionActionId;
    }

    public String getProductionName() {
        return productionName;
    }

    public void setProductionName(String productionName) {
        this.productionName = productionName;
    }

    public int getProductionImage() {
        return productionImage;
    }

    public void setProductionImage(int productionImage) {
        this.productionImage = productionImage;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }
}
