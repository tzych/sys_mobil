package com.example.myapplication.model;

import java.util.Arrays;
import java.util.List;

public enum ActionType {
    START_OF_PRODUCTION("Start of Production"),
    ADDING_SUGAR("Adding sugar"),
    CHECKING_SUGAR("Checking BLG"),
    REMOVING_FRUITS("Fruits removal"),
    END_OF_PRODUCTION("Production end"),
    OTHER("Other");

    private String desc;
    ActionType(String s) {
        desc = s;
    }

    public String getDesc() {
        return desc;
    }
}
