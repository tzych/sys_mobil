package com.example.myapplication.model;

public enum ActionStatus {
    TO_DO("To do"),
    DONE("Done");

    private String status;
    ActionStatus(String s) {
        status = s;
    }

    public String getDesc() {
        return status;
    }
}
